# install docker
apt-get update -y && apt-get upgrade -y

sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

sudo apt-get update

sudo apt install docker-ce -y

# install docker compose

sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version

# set .env
. production.env

# make dirs writable
chmod a+rwx -R $PATH_TO_STORAGE_DIR
# chown -R 13001:13001 $PATH_TO_STORAGE_DIR/youtrack
